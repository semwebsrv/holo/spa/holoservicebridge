import "./set-public-path";
import React from "react";

// Inspiration at https://github.com/react-microfrontends/api/blob/master/src/fetchWithCache.js

// const baseURL = "https://swapi.co/api/";
//
// const axiosInstance = axios.create({
//   baseURL,
//   timeout: 20000
// });

export const serviceBridgeStore = {
  loggedIn: false,
  user: {
    id: null,
  },
  applications: {},
};

export const StoreContext = React.createContext(serviceBridgeStore);

export const useServiceBridgeStore = () => {
  const store = React.useContext(StoreContext);
  if (!store) {
    // this is especially useful in TypeScript so you don't need to be checking for null all the time
    throw new Error("useStore must be used within a StoreProvider.");
  }
  return store;
};

// Anything exported from this file is importable by other in-browser modules.
export function publicApiFunction() {}
